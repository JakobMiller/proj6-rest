# Laptop Service
import os
from pymongo import MongoClient

# Instantiate the app
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

def fill(self):
    brevets = {['Checkpoint': '0km', 'open_time': '10:00', 'close_time': '11:00'],['Checkpoint': '100km', 'open_time' : '12:56', 'close_time' : '16:40'], ['checkpoint': '200km', 'open_time' : '15:53', 'close_time' : '23:20'], ['checkpoint': '300km', 'open_time': '19:00', 'close_time': '06:00'], ['checkpoint': '400km', 'open_time': '22:08', 'close_time': '12:40'], ['checkpoint': '500km', 'open_time': '01:28', 'close_time': '19:20'], ['checkpoint': '600km', 'open_time': '04:48', 'close_time': '02:00'], ['checkpoint': '700km', 'open_time': '08:22', 'close_time': '10:45'], ['checkpoint': '800km', 'open_time': '11:57', 'close_time': '19:30'], ['checkpoint': '900km', 'open_time': '15:31', 'close_time': '04:15'], ['checkpoint': '1000km', 'open_time': '19:05', 'close_time': '13:00']}

    for entry in brevets:
        db.tododb.insert_one(entry)
    return

# Run the application
if __name__ == '__main__':
    fill()
    return 0
