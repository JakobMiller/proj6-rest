# Laptop Service
import os
from flask import Flask, redirect, url_for, request
from pymongo import MongoClient
from flask_restful import Resource, Api

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class All(Resource):
    def get(self):
        entries = [entry for entry in db.tododb.find()]
        return entries

# Create routes
# Another way, without decorators
api.add_resource(All, '/')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
